/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdarg>
#include <gtest/gtest.h>

#include "hitrace/hitracec.h"
#include "file_utils.h"

using namespace testing::ext;
using namespace std;

class HitraceCtest : public testing::Test {
public:

    static void SetUpTestCase();
    static void TearDownTestCase();
    void SetUp();
    void TearDown();

private:
};
void HitraceCtest::SetUp()
{
}
void HitraceCtest::TearDown()
{
}
void HitraceCtest::SetUpTestCase()
{
}
void HitraceCtest::TearDownTestCase()
{
}

/**
 * @tc.name HiTrace C interface test
 * @tc.number DFX_DFT_HiTrace_0001
 * @tc.desc HiTrace C interface test
*/
HWTEST_F(HitraceCtest, Hitrace_test, Function|MediumTest|Level1)
{
    GTEST_LOG_(INFO) << "Hitrace_test start" << endl;
    HiTraceIdStruct hiTraceId;
    HiTraceFlag hitraceflag = HITRACE_FLAG_INCLUDE_ASYNC;
    hiTraceId = HiTraceBegin("test", HITRACE_FLAG_INCLUDE_ASYNC);

    HiTraceSetId(&hiTraceId);
    hiTraceId = HiTraceGetId();
    ASSERT_TRUE(HiTraceIsValid(&hiTraceId));

    HiTraceEnableFlag(&hiTraceId, hitraceflag);
    ASSERT_TRUE(HiTraceIsFlagEnabled(&hiTraceId, hitraceflag));

    int hitraceflags = HITRACE_FLAG_FAULT_TRIGGER;
    HiTraceSetFlags(&hiTraceId, hitraceflags);
    hitraceflags = HiTraceGetFlags(&hiTraceId);
    ASSERT_TRUE(hitraceflags == HITRACE_FLAG_FAULT_TRIGGER);

    uint64_t chainId = 10000;
    HiTraceSetChainId(&hiTraceId, chainId);
    chainId = HiTraceGetChainId(&hiTraceId);
    ASSERT_TRUE(chainId==10000);

    uint64_t spanId = 12345678;
    HiTraceSetSpanId(&hiTraceId, spanId);
    spanId = HiTraceGetSpanId(&hiTraceId);
    ASSERT_TRUE(spanId == 12345678);

    HiTraceIdStruct childId = HiTraceCreateSpan();
    ASSERT_TRUE(HiTraceIsValid(&childId));

    /* set child id to thread id */
    HiTraceSetId(&childId);

    /* continue to create child span */
    HiTraceIdStruct grandChildId = HiTraceCreateSpan();
    ASSERT_TRUE(HiTraceIsValid(&grandChildId));
    EXPECT_EQ(HiTraceGetFlags(&grandChildId), HiTraceGetFlags(&childId));
    EXPECT_EQ(HiTraceGetChainId(&grandChildId), HiTraceGetChainId(&childId));
    EXPECT_EQ(HiTraceGetParentSpanId(&grandChildId), HiTraceGetSpanId(&childId));

    HiTraceSetChainId(&grandChildId, chainId);
    chainId = HiTraceGetChainId(&grandChildId);
    ASSERT_TRUE(chainId==10000);

    HiTraceSetSpanId(&grandChildId, spanId);
    spanId = HiTraceGetSpanId(&grandChildId);
    ASSERT_TRUE(spanId==12345678);
    HiTraceClearId();
    HiTraceEnd(&hiTraceId);
    GTEST_LOG_(INFO) << "Hitrace_test end" << endl;
}