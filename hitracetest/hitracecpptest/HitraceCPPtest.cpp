/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdarg>
#include <ctime>
#include <gtest/gtest.h>
#include <pthread.h>

#include "hitrace/hitrace.h"
#include "hitrace/hitraceid.h"
#include "hitrace/trace.h"
#include "file_utils.h"

using namespace OHOS;
using namespace HiviewDFX;
using namespace testing;
using namespace testing::ext;
using namespace std;

class HitraceCPPtest : public testing::Test {
public:

    static void SetUpTestCase();
    static void TearDownTestCase();
    void SetUp();
    void TearDown();

private:
};
void HitraceCPPtest::SetUp()
{
}
void HitraceCPPtest::TearDown()
{
}
void HitraceCPPtest::SetUpTestCase()
{
}
void HitraceCPPtest::TearDownTestCase()
{
}
/**
 * @tc.name HiTrace C++ interface test
 * @tc.number DFX_DFT_HiTrace_CPP_0001
 * @tc.desc HiTrace C++ interface test
 */
HWTEST_F(HitraceCPPtest, Hitrace_cpptest, Function|MediumTest|Level1)
{
    GTEST_LOG_(INFO) << "Hitrace_cpptest start" << endl;
    HiTraceFlag hitraceflag = HITRACE_FLAG_INCLUDE_ASYNC;
    HiTraceId hiTraceId = HiTrace::Begin("test1", HITRACE_FLAG_INCLUDE_ASYNC);

    HiTrace::SetId(hiTraceId);
    hiTraceId = HiTrace::GetId();
    ASSERT_TRUE(hiTraceId.IsValid());

    hiTraceId.EnableFlag(hitraceflag);
    ASSERT_TRUE(hiTraceId.IsFlagEnabled(hitraceflag));

    int hitraceflags = HITRACE_FLAG_FAULT_TRIGGER;
    hiTraceId.SetFlags(hitraceflags);
    hitraceflags = hiTraceId.GetFlags();
    ASSERT_TRUE(hitraceflags == HITRACE_FLAG_FAULT_TRIGGER);

    uint64_t chainId = 10000;
    hiTraceId.SetChainId(chainId);
    chainId = hiTraceId.GetChainId();
    ASSERT_TRUE(chainId == 10000);

    uint64_t spanId = 12345678;
    hiTraceId.SetSpanId(spanId);
    spanId = hiTraceId.GetSpanId();
    ASSERT_TRUE(spanId == 12345678);

    HiTraceId childId = HiTrace::CreateSpan();
    EXPECT_EQ(1, childId.IsValid());

    /* set child id to thread id */
    HiTrace::SetId(childId);
    /* continue to create child span */
    HiTraceId grandChildId = HiTrace::CreateSpan();
    EXPECT_EQ(1, grandChildId.IsValid());
    EXPECT_EQ(grandChildId.GetFlags(), childId.GetFlags());
    EXPECT_EQ(grandChildId.GetChainId(), childId.GetChainId());
    EXPECT_EQ(grandChildId.GetParentSpanId(), childId.GetSpanId());

    grandChildId.SetChainId(chainId);
    chainId = grandChildId.GetChainId();
    ASSERT_TRUE(chainId == 10000);

    grandChildId.SetSpanId(spanId);
    spanId = grandChildId.GetSpanId();
    ASSERT_TRUE(spanId == 12345678);
    HiTrace::ClearId();
    HiTrace::End(hiTraceId);
    GTEST_LOG_(INFO) << "Hitrace_cpptest end" << endl;
}